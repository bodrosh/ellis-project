<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    // полчаем список услуг
    public static function getAllServices() {
        return Service::paginate(3);
        // return Material::limit(3)->get();
    }
}
