<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
// полчаем список материалов
    public static function getAllOrders() {
        // return Order::paginate(3);
        return Order::orderBy('start_date', 'desc')->get();
    }

    // получаем список материалов заказа
    public function materials() {
        return $this->belongsToMany('App\Material')->withPivot('count');
    }

    // получаем филиал, в кот. осуществлен заказ
    public function franchise() {
        return $this->belongsTo('App\Franchise');
    }

}
