<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Franchise extends Model
{
    protected $fillable = ['name', 'address', 'description'];

    // получаем сотрудников филиала
    public function users() {
        return $this->hasMany('App\User');
    }

    // полчаем список филиалов
    public static function getAllFranchises() {
        return Franchise::all();
    }
}
