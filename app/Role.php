<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'slug',
    ];

    // получаем пользователей с данными правами доступа
    public function user() {
        return $this->belongsToMany('App\User');
    }
}
