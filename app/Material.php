<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    // полчаем список материалов
    public static function getAllMaterials() {
        return Material::paginate(3);
       // return Material::limit(3)->get();
    }

    // получаем список заказов, в которых используется данный материал
    public function orders() {
        return $this->belongsToMany('App\Order');
    }
}
