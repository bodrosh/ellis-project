<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectByRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // если одна из ролей пользователя admin, пускаем
            foreach (Auth::user()->roles as $role) {
                if($role->slug == 'admin') {
                    return $next($request);
                }
            }
        }

        return redirect('/home');
    }
}
