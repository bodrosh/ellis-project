<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Franchise;
use App\Material;
use App\Service;
use App\Order;


class DashboardController extends Controller
{
    // главная страница админки
    public function dashboard() {
        return view('admin.index', [
            'users' => User::getAllUsers(),
            'count_users' => User::count(),
            'franchises' => Franchise::getAllFranchises(),
            'count_franchises' => Franchise::count(),
            'materials' => Material::getAllMaterials(),
            'count_materials' => Material::count(),
            'services' => Service::getAllServices(),
            'count_services' => Service::count(),
            'orders' => Order::getAllOrders(),
            'count_orders' => Order::count(),
        ]);
    }

    //данные для построения графика
    public function dataChart() {
        return [
            'labels' => ['март', 'апрель', 'май', 'июнь'],
            'datasets' => array([
                'label' => 'Продажи',
                'backgroundColor' => '#ec3',
                'data' => [11500, 1800, 5000, 2000],
            ])
        ];
    }

}
