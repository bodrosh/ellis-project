@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-sm-12">

                <a href="{{route('admin.user_managment.user.create')}}" class="btn btn-primary pull-right"> Создать
                    заказ</a>
                <p></p>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">№ заказа</th>
                        <th scope="col">Франчази</th>
                        <th scope="col">Дата начала</th>
                        <th scope="col">Дата завершения</th>
                        <th scope="col">Изменить</th>
                        <th scope="col">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->franchise->name}}</td>
                            <td>{{$order->start_date}}</td>
                            <td>{{$order->finish_date}}</td>
                            {{--<td>{{$user->roles->implode('name', ', ') }}</td>--}}
                            <td>
                                <a href="{{route('admin.user_managment.user.edit', $order)}}">
                                    <i class="far fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                {{--<form action="{{route('admin.user_managment.user.destroy', $order)}}" method="post"--}}
                                {{--onsubmit="if(confirm('Удалить?')) { return true } else { return false }">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<input type="hidden" name="_method" value="delete" >--}}
                                {{--{{method_field('DELETE')}}--}}
                                {{--<button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>--}}
                                {{--</form>--}}
                                <a href="#"><i class="far fa-trash-alt"></i></a>

                            </td>


                        </tr>
                        <tr>
                            <td colspan="6">
                                <table class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">№ материала</th>
                                        <th scope="col">Наименование</th>
                                        <th scope="col">Цена</th>
                                        <th scope="col">Количество</th>
                                        <th scope="col">Сумма</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php ($order_price = 0)
                                    @forelse($order->materials as $material)
                                        <tr>
                                            <th scope="row">{{$material->id}}</th>
                                            <td>{{$material->name}}</td>
                                            <td>{{$price = $material->client_price}}</td>
                                            <td>{{$count = $material->pivot->count}}</td>

                                            @php ($order_price += $price * $count)

                                            <td>{{$price * $count}}</td>
                                        </tr>
                                    @empty
                                        <h6>Материалы данного заказа не добавлены</h6>
                                    @endforelse
                                    <tr>
                                        <td colspan="4"><strong>Итого:</strong></td>
                                        <td>{{$order_price}}</td>
                                    </tr>


                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$orders->links()}}

            </div>
        </div>
    </div>
@endsection
