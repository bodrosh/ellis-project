@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-sm-4">
            <h6><i class="fas fa-users"></i> <strong>Сотрудники</strong> ({{$count_users}})</h6>
            <ul class="list-group">
                @foreach($users as $user)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        {{$user->name}}
                         <span class="badge badge-primary badge-pill">
                                {{$user->roles->implode('name', ', ') }}
                            </span>
                    </li>
                @endforeach
            </ul>
            <p> </p>

            <a href="{{ route('admin.user_managment.user.index') }}">
            <button type="button" class="btn btn-primary">
                    Все сотрудники <span class="badge badge-light">{{$count_users}}</span>
                </button>
            </a>
<p>
            <hr>
            <h6><i class="fas fa-city"></i> <strong>Франчази</strong> ({{$count_franchises}})</h6>
            <ul class="list-group">
                @foreach($franchises as $franchise)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        {{$franchise->name}}
                        <a href="#"> <span class="badge badge-primary badge-pill">
                            <i class="far fa-edit"></i>
                            </span></a>
                    </li>
                @endforeach
            </ul>



            <p> </p>

            <hr>
            <h6><i class="far fa-calendar-check"></i> <strong>Последние заказы</strong> ({{$count_orders}})</h6>
            <ul class="list-group">
                @foreach($orders as $order)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="#">Заказ #{{$order->id}}</a>
                         <span class="badge badge-primary badge-pill">
                            {{--{{$order->start_date}}--}}
                                {{ \Carbon\Carbon::parse($order->start_date)->format('d.m.Y')}}
                            </span>
                    </li>
                @endforeach
            </ul>

            <p> </p>

            <a href="{{ route('admin.order.index') }}">
                <button type="button" class="btn btn-primary">
                    Все заказы <span class="badge badge-light">{{$count_orders}}</span>
                </button>
            </a>



        </div>

        <div class="col-sm-8">
            <h6><i class="fab fa-accusoft"></i> <strong>Материалы</strong> ({{$count_materials}})</h6>

            <ul class="list-group">
                @foreach($materials as $material)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        {{$material->name}}
                        <a href="#"> <span class="badge badge-primary badge-pill">
                            {{$material->original_price}} руб. -> {{$material->client_price}} руб. / {{$material->unit}}
                            </span></a>
                    </li>
                @endforeach
            </ul>
            <p>
                {{$materials->links()}}
            </p>
            <p>
            <hr>
            <h6><i class="fas fa-hammer"></i> <strong>Услуги</strong> ({{$count_services}})</h6>
            <ul class="list-group">
                @foreach($services as $service)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        {{$service->name}}
                        <a href="#"> <span class="badge badge-primary badge-pill">
                            {{$service->price}} руб. / {{$service->unit}}
                            </span></a>
                    </li>
                @endforeach
            </ul>
            <p>
                {{$materials->links()}}
            </p>

            <chart-component></chart-component>
        </div>
</div>
@endsection
