@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-sm-12">

                <a href="{{route('admin.user_managment.user.create')}}" class="btn btn-primary pull-right"> Добавить
                    сотрудника</a>
                <p></p>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Имя</th>
                        <th scope="col">email</th>
                        <th scope="col">Офис</th>
                        <th scope="col">Должность</th>
                        <th scope="col">Изменить</th>
                        <th scope="col">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <th scope="row">1</th>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->franchise->name}}</td>
                            <td>{{$user->roles->implode('name', ', ') }}</td>
                            <td>
                                <a href="{{route('admin.user_managment.user.edit', $user)}}">
                                    <i class="far fa-edit"></i>
                                 </a>
                            </td>
                            <td>
                                <form acti on="{{route('admin.user_managment.user.destroy', $user)}}" method="post"
                                      onsubmit="if(confirm('Удалить?')) { return true } else { return false }">
                                    {{ csrf_field() }}
                                    {{--<input type="hidden" name="_method" value="delete" >--}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$users->links()}}

            </div>
        </div>
    </div>
@endsection
